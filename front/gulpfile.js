'use sctrict';

const postcss =       require('gulp-postcss');
const inlineSvg =     require('postcss-inline-svg');
const gulp =          require('gulp');
const autoprefixer =  require('autoprefixer');
const cssnano =       require('cssnano');
// const server =        require('browser-sync');
const browserSync =   require('browser-sync').create();
const sass =          require('gulp-sass');
const uglify =        require('gulp-uglify');
const del =           require('del');
const rigger =        require('gulp-rigger');
const rename =        require('gulp-rename');
const concat =        require('gulp-concat');
const path =          require('path');
const flatten =       require('gulp-flatten');
const newer =         require('gulp-newer');
const debug =         require('gulp-debug');
const remember =      require('gulp-remember');


// Configure the web server.
// var config = {
//   server: {baseDir: './dist'},
//   tunnel: false,
//   port: 9100
// };

// Collection of file paths.
const paths = {
  // The path to the file source.
  src: {
    vendor:   './src/vendor/**/*.*',
    fonts:    './src/fonts/**/*.*',
    sass:     './src/sass/styles.scss',
    img:      './src/img/**/*.*',
    js:       './src/js/index.js',
    html:     './src/html/*.html',
    svg:      './src/svg/*.svg',
  },
  // The paths to the directories of the release project.
  build: {
    vendor:   './dist/vendor',
    fonts:    './dist/fonts',
    sass:     './dist',
    img:      './dist/img',
    base:     './dist/**/*',
    js:       './dist/js',
    html:     './dist',
    svg:      './dist/svg',
  },
  // The path to the files whose changes you want to monitor.
  watch: {
    vendor:   './src/vendor/**/*.*',
    fonts:    './src/fonts/**/*.*',
    sass:     './src/sass/**/*.scss',
    img:      './src/img/**/*.*',
    js:       './src/js/**/*.js',
    html:     './src/html/**/*.html',
    svg:      './src/svg/**/*.svg',
  }
};



// Build tasks.
gulp.task('build:html', function() {
  return gulp.src(paths.src.html)
    .pipe(rigger())
    .pipe(flatten())
    .pipe(gulp.dest(paths.build.html));
});

gulp.task('build:sass', function() {
  return gulp.src(paths.src.sass)
    .pipe(sass())
    .pipe(postcss([
      inlineSvg({path:'./dist/svg'}),
      autoprefixer({ browsers: ['last 3 version'], cascade: false }),
      cssnano
    ]))
    // .pipe(remember('build:sass'))
    .pipe(rename("template_styles.css"))
    .pipe(flatten())
    .pipe(gulp.dest(paths.build.sass));
});

gulp.task('build:js', function() {
  return gulp.src([
      './src/js/app/*.js',
      paths.src.js
    ])
    .pipe(concat('index.js'))
    // .pipe(uglify())
    .pipe(flatten())
    .pipe(gulp.dest(paths.build.js));
});

gulp.task('build:img', function() {
  return gulp.src(paths.src.img)
    .pipe(flatten())
    .pipe(newer(paths.build.img))
    .pipe(debug({title: 'Images copied:'}))
    .pipe(gulp.dest(paths.build.img));
});

gulp.task('build:svg', function() {
  return gulp.src(paths.src.svg)
    .pipe(flatten())
    .pipe(newer(paths.build.svg))
    .pipe(debug({title: 'SVGs copied:'}))
    .pipe(gulp.dest(paths.build.svg));
});

gulp.task('build:fonts', function() {
  return gulp.src(paths.src.fonts, {since: gulp.lastRun('build:fonts')})
    .pipe(flatten())
    .pipe(newer(paths.build.fonts))
    .pipe(debug({title: 'Fonts copied:'}))
    .pipe(gulp.dest(paths.build.fonts));
});

gulp.task('build:vendor', function() {
  return gulp.src([
      './src/vendor/jquery-1.11.3.min.js',
      './src/vendor/*.js'
    ])
    .pipe(concat('jquery-plugins.min.js'))
    .pipe(uglify())
    .pipe(flatten())
    .pipe(gulp.dest(paths.build.vendor));
});

gulp.task('build', gulp.parallel('build:html', 'build:sass', 'build:fonts', 'build:img', 'build:svg', 'build:js', 'build:vendor'));


// Monitoring tasks.
gulp.task('watch', function(done) {
  gulp.watch(paths.watch.html, gulp.parallel('build:html'));
  gulp.watch(paths.watch.js, gulp.parallel('build:js'));
  gulp.watch(paths.watch.sass, gulp.parallel('build:sass'));
  gulp.watch(paths.watch.img, gulp.parallel('build:img'));
  gulp.watch(paths.watch.svg, gulp.parallel('build:svg'));
  gulp.watch(paths.watch.fonts, gulp.parallel('build:fonts'));
  gulp.watch(paths.watch.vendor, gulp.parallel('build:vendor'));
  done();
});


// other tasks.
// gulp.task('server', function(done) {
//   server(config, done);
// });

// server task
gulp.task('server', function() {
  browserSync.init({
    server: './dist'
  });
  browserSync.watch('./dist/**/*.*').on('change', browserSync.reload);
});


// clean task
gulp.task(function clean() {
  return del([
    paths.build.base,
    '!./dist/js-helpers',
    '!./dist/js-helpers/*.js'
  ]);
});



// The default task.
gulp.task('default',
  gulp.series('build', gulp.parallel('server', 'watch'))
);

// Default task width clean
gulp.task('build:hard',
  gulp.series('clean','build', gulp.parallel('server', 'watch'))
);
