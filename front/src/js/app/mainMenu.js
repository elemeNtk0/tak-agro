var mainMenu = (function() {
  var me = {},
      menu,
      menuBtn;

  me.init = function() {
    menu = document.querySelector('#js-mainMenu');
    menuBtn = document.querySelector('#js-mainMenuBtn');

    menuBtn.addEventListener('click', toggleClassMenu, false);
  };


  function toggleClassMenu() {
    if(!menu.classList.contains('main-menu--is-open')) {
      menu.classList.add('main-menu--is-open');
      menuBtn.classList.add('main-menu__btn--is-active');
    } else {
      menu.classList.remove('main-menu--is-open');
      menuBtn.classList.remove('main-menu__btn--is-active');
    }
  }



  return me;
}());




