var body = document.body;
var timer;

window.addEventListener('scroll', function() {
  clearTimeout(timer);
  if(!body.classList.contains('disable-hover')) {
    body.classList.add('disable-hover')
  }
  timer = setTimeout(function() {
    body.classList.remove('disable-hover')
  },200);
}, false);
