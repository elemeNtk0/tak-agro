
$(document).ready(function() {

  $('.js-partners').slick({
    autoplay: false,
    arrows: true,
    autoplaySpeed: 2000,
    prevArrow: '<button type="button" class="slick-prev">Пролистать назад</button>',
    nextArrow: '<button type="button" class="slick-next">Пролистать вперёд</button>',
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 801,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });

  $('#js-mainMenu').length && mainMenu.init();

  $('.js-tabs').length && tabs.init();

  function productSlideInit() {
    var productSlider = $('#js-product-slider').lightSlider({
      gallery: true,
      controls: false,
      item: 1,
      loop: false,
      slideMargin: 0,
      thumbMargin: 30,
      thumbItem: 3, useCss: true,
    });

    $('#btnPoductSliderPrev').on('click', function () {
      productSlider.goToPrevSlide();
    });
    $('#btnPoductSliderNext').on('click', function () {
      productSlider.goToNextSlide();
    });
  }
  var sliderActivation = 0;

  if ($('.js-hidden-slider').css('display') == 'none')
    $('.js-tabsHiddenSlider').on('click', function () {
      if (sliderActivation >= 1) $('#js-product-slider').refresh;
      else productSlideInit();
      // $('#js-product-slider').rebuild;
      sliderActivation++;
    });
  else productSlideInit();


  $(window).resize(function() {
    $('#js-product-slider').refresh;
  });

}); //.doc-ready







