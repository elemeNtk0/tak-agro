<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$curPage = $APPLICATION->GetCurPage(true);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!--<link rel="shortcut icon" type="image/x-icon" href="<?//=SITE_DIR?>/favicon.ico" />-->
  <link rel="icon" href="/favicon.png" type="image/png">
  <link rel="shortcut icon" href="/favicon.png" type="image/png">
  <?//$APPLICATION->ShowHead(); ?>




  <?
  // Возможно надо будет вернуть подключение
  // $APPLICATION->ShowHeadStrings();
  // $APPLICATION->ShowHeadScripts();
  $APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/template_styles.css");

  $APPLICATION->AddHeadScript("/bitrix/templates/.default/vendor/jquery-plugins.min.js");
  $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/index.js");
  ?>
  <?$APPLICATION->ShowHead()?>
  <title><?$APPLICATION->ShowTitle()?></title>
</head>


<body class="layout <?if ($curPage == SITE_DIR."index.php"):?> layout--main-page<?endif?>">
<?$APPLICATION->ShowPanel();?>


  <header class="header">
    <div class="header__container">
      <a class="header__logo" href="<?=SITE_DIR?>" alt="На главную">
        <img src="/bitrix/templates/.default/img/logo-takagro.svg" width="169" height="98" alt="Tag-Agro">
      </a>

      <div class="header__menu">
        <div class="main-menu" id="js-mainMenu">
          <button class="main-menu__btn" id="js-mainMenuBtn" title="Показать/Скрыть меню"><span>Показать/скрыть меню</span></button>


          <?//Само меню?>
          <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "top",
            Array(
              "COMPONENT_TEMPLATE" => "top",
              "ROOT_MENU_TYPE" => "top",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(""),
              "MAX_LEVEL" => "1",
              "CHILD_MENU_TYPE" => "left",
              "USE_EXT" => "N",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "N"
            )
          );?>
        </div>
      </div>

      <div class="header__contacts  contacts-block">
        <div class="contacts-block__title">
          <?$APPLICATION->IncludeComponent(
            "bitrix:main.include", ".default",
            array(
              "AREA_FILE_SHOW" => "file",
              "PATH" => "/bitrix/templates/.default/__include/main-phones-title.php",
              "COMPONENT_TEMPLATE" => ".default", "EDIT_TEMPLATE" => ""
            ), false
          );?>
        </div>
        <div class="contacts-block__phone  contacts-block__phone--top">
          <?$APPLICATION->IncludeComponent(
            "bitrix:main.include", ".default",
            array(
              "AREA_FILE_SHOW" => "file",
              "PATH" => "/bitrix/templates/.default/__include/main-phones.php",
              "COMPONENT_TEMPLATE" => ".default", "EDIT_TEMPLATE" => ""
            ), false
          );?>
        </div>
      </div>
    </div>
  </header>


<?// Промо на главной странице ?>
<?if ($curPage == SITE_DIR."index.php"):?>
  <div class="promo">
    <h1 class="promo__title">
      <?$APPLICATION->IncludeComponent(
        "bitrix:main.include", ".default",
        array(
          "AREA_FILE_SHOW" => "file",
          "PATH" => "/bitrix/templates/.default/__include/promo__title.php",
          "COMPONENT_TEMPLATE" => ".default", "EDIT_TEMPLATE" => ""
        ), false
      );?>
    </h1>
    <span class="promo__subtitle">
      <?$APPLICATION->IncludeComponent(
        "bitrix:main.include", ".default",
        array(
          "AREA_FILE_SHOW" => "file",
          "PATH" => "/bitrix/templates/.default/__include/promo__subtitle.php",
          "COMPONENT_TEMPLATE" => ".default", "EDIT_TEMPLATE" => ""
        ), false
      );?>
    </span>
  </div>
<?endif?>


  <main class="layout__content  main-content">
    <div class="container">
      <?//Все остальные страницы?>
      <?if ($curPage != SITE_DIR."index.php"):?>
        <h1><?$APPLICATION->ShowTitle(false);?></h1>
      <?endif?>
      <!-- ВСТАВЛЯЕМАЯ ОБЛАСТЬ -->
