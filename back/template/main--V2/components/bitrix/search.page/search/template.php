<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="inner">
	<h1>Результаты поиска</h1>
<?if(count($arResult["SEARCH"])>0):?>
	<div class="product-list"><?foreach($arResult["ITEMS"] as $cell=>$arItem):?><div class="item<?if($cell%4==3):?> nml<?endif;?>"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
	<img src="<?=$arResult["FILES"][$arItem["PREVIEW_PICTURE"]]?>" alt="<?=$arItem["NAME"]?>" />
	<span><?=$arItem["NAME"]?></span>
	</a></div><?endforeach;?></div>
	<?=$arResult["NAV_STRING"]?>
<?else:?>
	<?ShowNote("Ничего не найдено");?>
<?endif;?>
</div>
