<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(count($arResult["SEARCH"])>0 && CModule::IncludeModule("iblock")){
	foreach($arResult["SEARCH"] as $arItem)
		$arID[] = $arItem['ITEM_ID'];
	
	$arResult["ITEMS"] = array();
	$rsElement = CIBlockElement::GetList(array(), array("ACTIVE"=>"Y","ID"=>$arID), false, false, array("ID","NAME","DETAIL_PAGE_URL","PREVIEW_PICTURE"));
	while($arElement = $rsElement->GetNext()){
		$arResult["FILES"][$arElement["PREVIEW_PICTURE"]] = "";
		$arResult["ITEMS"][] = $arElement;
	}
	
	if(!empty($arResult["FILES"])){
	
		$upload = COption::GetOptionString("main", "upload_dir");
		$rsFile = CFile::GetList(array(), array("@ID"=>implode(",",array_keys($arResult["FILES"]))));
		while($arFile = $rsFile->GetNext())
			$arResult["FILES"][$arFile["ID"]] = "/".$upload."/".$arFile["SUBDIR"]."/".$arFile["FILE_NAME"];
	
	}

}
?>