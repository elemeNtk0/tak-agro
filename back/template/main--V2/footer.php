<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

    <?//Партнёры на главной?>
    <?if ($curPage == SITE_DIR."index.php"):?>
      <!-- ЭТО СТАТИКА, БРО -->
      <section class="box  box--big-top">
        <h2 class="page-title  page-title--section">Наши партнёры</h2>

        <?$APPLICATION->IncludeComponent(
          "bitrix:news.list",
          "main-page__partners",
          Array(
            "COMPONENT_TEMPLATE" => "main-page__partners",
            "IBLOCK_TYPE" => "TAK",
            "IBLOCK_ID" => "9",
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => array(0=>"PREVIEW_PICTURE",1=>"",),
            "PROPERTY_CODE" => array(0=>"",1=>"",),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "N",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N"
          )
        );?>
      </section>
    <?endif?>

    </div>
  </main>



  <footer class="footer">
    <div class="container">
      <div class="footer__container">
        <a class="footer__logo" href="<?=SITE_DIR?>" alt="На главную">
          <img src="/bitrix/templates/.default/img/logo-takagro.svg" width="169" height="98" alt="Tak-Agro">
        </a>

        <?//Footer MENU?>
        <?$APPLICATION->IncludeComponent(
          "bitrix:menu",
          "footer-menu",
          Array(
            "COMPONENT_TEMPLATE" => "footer-menu",
            "ROOT_MENU_TYPE" => "footer-menu",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => array(),
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "",
            "USE_EXT" => "N",
            "DELAY" => "Y",
            "ALLOW_MULTI_SELECT" => "N"
          )
        );?>

        <div class="footer__contacts  contacts-block">
          <div class="contacts-block__title">
            <?$APPLICATION->IncludeComponent(
              "bitrix:main.include", ".default",
              array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/bitrix/templates/.default/__include/main-phones-title.php",
                "COMPONENT_TEMPLATE" => ".default", "EDIT_TEMPLATE" => ""
              ), false
            );?>
          </div>
          <div class="contacts-block__phone  contacts-block__phone--top">
            <?$APPLICATION->IncludeComponent(
              "bitrix:main.include", ".default",
              array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/bitrix/templates/.default/__include/main-phones.php",
                "COMPONENT_TEMPLATE" => ".default", "EDIT_TEMPLATE" => ""
              ), false
            );?>
          </div>
        </div>
      </div>
    </div>
  </footer>

</body>
</html>
