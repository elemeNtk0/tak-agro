<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="left-menu">
<?if (!empty($arResult)):?>
<ul class="section">
<?
$previousLevel = 0;
foreach($arResult as $arItem):?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>
		<li><a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?>class="selected"<?endif?>><?=$arItem["TEXT"]?></a>
			<ul>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?>class="selected"<?endif?>><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
<?endforeach?>
<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
</ul>
<?endif?>
</div>