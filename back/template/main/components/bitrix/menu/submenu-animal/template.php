<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div class="submenu animal wrap980">
	<ul><?foreach($arResult as $arItem):?><?if(!$arItem["SELECTED"]):?><li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li><?else:?><li class="active"><?=$arItem["TEXT"]?></li><?endif;?><?endforeach?></ul>
	<div></div>
</div>
<?endif?>