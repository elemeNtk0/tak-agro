<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?
	$APPLICATION->SetAdditionalCSS("/styles/style.css");
	
	$APPLICATION->AddHeadScript('/script/jquery-1.11.0.min.js');
	$APPLICATION->AddHeadScript('/script/jquery.cycle2.min.js');
?>
	<?$APPLICATION->ShowHead()?>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<div class="wrapper">
	<div class="header">
		<div class="top"><div class="wrap980">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
				"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
				"MAX_LEVEL" => "1",	// Уровень вложенности меню
				"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
				"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				"MENU_CACHE_TYPE" => "A",	// Тип кеширования
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
				),
				false
			);?>
			<div class="search">
				<?$APPLICATION->IncludeComponent("bitrix:search.form", "top", array(
					"PAGE" => "#SITE_DIR#search/"
					),
					false
				);?>
			</div>
		</div></div>
		<div class="bottom wrap980">
			<?$APPLICATION->IncludeComponent(
				"tak:slider",
				"",
				Array(
					"IBLOCK_ID" => "1",
					"SORT_BY" => "SORT",
					"SORT_ORDER" => "ASC",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3000"
				),
			false
			);?>
			<a href="/"></a>
			<div class="address"><?$APPLICATION->IncludeFile("/___include/header_address.php",Array(),Array("MODE"=>"html"));?></div>
		</div>
	</div><!-- .header-->
	<div class="main-menu wrap980">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "middle", Array(
			"ROOT_MENU_TYPE" => "mid",	// Тип меню для первого уровня
			"MAX_LEVEL" => "1",	// Уровень вложенности меню
			"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
			"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N",	// Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
			"MENU_CACHE_TYPE" => "A",	// Тип кеширования
			"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
			"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
			),
			false
		);?>
	</div>
<?if($APPLICATION->GetDirProperty("menu_tech") == "Y"):?>
	<?$APPLICATION->IncludeComponent("bitrix:menu", "submenu-tech", Array(
		"ROOT_MENU_TYPE" => "tech",	// Тип меню для первого уровня
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		),
		false
	);?>	
<?endif;?>
<?if($APPLICATION->GetDirProperty("menu_animal") == "Y"):?>
	<?$APPLICATION->IncludeComponent("bitrix:menu", "submenu-animal", Array(
		"ROOT_MENU_TYPE" => "animal",	// Тип меню для первого уровня
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		),
		false
	);?>	
<?endif;?>
	<div class="content wrap980 <?=$APPLICATION->GetDirProperty("class")?>">
		